// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { RaceType } from './../../../src/models/characters/common/race-type';
import { ICharacter } from './../../../src/models/characters/contracts/character';
import { Warrior } from './../../../src/models/characters/warrior';
describe('Warrior', () => {
    describe('Constructor should', () => {
        it('correctly assign passed values', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Terrestrial;

            // Act
            const warrior: Warrior = new Warrior(name, race);

            // Assert
            expect(warrior.name).toBe('Gosho');
            expect(warrior.race).toBe(RaceType.Terrestrial);
        });
    });
    describe('attack should', () => {
        it('throw when character is not alive.', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Alien;

            // Act
            const character: Warrior = new Warrior(name, race);
            character.isAlive = false;
            const target: ICharacter = new Warrior('Pesho', RaceType.Alien);
            // Assert
            expect(() => character.attack(target)).toThrow('Must be alive to perform this action.');
        });
        it('throw when character targets himself', () => {
             // Arrange
             const name: string = 'Gosho';
             const race: RaceType = RaceType.Alien;

             // Act
             const character: Warrior = new Warrior(name, race);

             // Assert
             expect(() => character.attack(character)).toThrow('Cannot attack self!');
        });
        it('throw when character targets himself', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Alien;

            // Act
            const character: Warrior = new Warrior(name, race);
            const target: Warrior = new Warrior('Pesho', RaceType.Alien);
            // Assert
            expect(() => character.attack(target)).toThrow('Cannot attack a character from the same race!');
       });
        it('Reduce health points of target', () => {
            // Arrange
            const atacker: Warrior = new Warrior('Pesho', RaceType.Alien);
            const target: Warrior = new Warrior('Pesho', RaceType.Terrestrial);

            // Act
            atacker.attack(target);
            const armorAfterAttack: number = target.armor;
            const healthAfterAttack: number = target.health;
            // Assert
            expect(target.health).toBe(healthAfterAttack);
            expect(target.armor).toBe(armorAfterAttack);
        });
    });
});
