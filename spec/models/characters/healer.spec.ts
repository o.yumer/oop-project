import { Heal } from './../../../src/commands/heal-command';
// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { Healer, RaceType, Warrior } from '../../../src/models';

describe('Warrior', () => {
    describe('Constructior should', () => {
        it('correctly assign passed values', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Terrestrial;

            // Act
            const warrior: Healer = new Healer(name, race);

            // Assert
            expect(warrior.name).toBe('Gosho');
            expect(warrior.race).toBe(RaceType.Terrestrial);
        });
    });
    describe('Heal should', () => {
        it('throw when character is not alive.', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Alien;

            // Act
            const character: Healer = new Healer(name, race);
            character.isAlive = false;
            const target: Warrior = new Warrior('Pesho', RaceType.Alien);
            // Assert
            expect(() => character.heal(target)).toThrow('Must be alive to perform this action.');
        });
        it('throw when target is not alive.', () => {
            // Arrange
            const name: string = 'Gosho';
            const race: RaceType = RaceType.Alien;

            // Act
            const character: Healer = new Healer(name, race);
            const target: Warrior = new Warrior('Pesho', RaceType.Alien);
            target.isAlive = false;
            // Assert
            expect(() => character.heal(target)).toThrow('Must be alive to perform this action.');
        });
        it('throw when character targets enemies', () => {
             // Arrange
             const name: string = 'Gosho';
             const race: RaceType = RaceType.Alien;

             // Act
             const character: Healer = new Healer(name, race);
             const target: Warrior = new Warrior('Pesho', RaceType.Terrestrial);
             // Assert
             expect(() => character.heal(target)).toThrow('A healer cannot heal enemy characters!');
        });

        it('Reduce health points of target', () => {
            // Arrange
            const healer: Healer = new Healer('Pesho', RaceType.Alien);
            const target: Warrior = new Warrior('Pesho', RaceType.Alien);
            target.health = 10;

            // Act
            healer.heal(target);
            const healthAfterHeal: number = target.health;

            // Assert
            expect(target.health).toBe(healthAfterHeal);

        });
    });
});
