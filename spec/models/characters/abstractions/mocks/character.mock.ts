import { Bag, Character, RaceType } from '../../../../../src/models';

export class CharacterMock extends Character {
    public _baseHealth: number = 100;

    public constructor(
        name: string,
        health: number,
        armor: number,
        abilityPoints: number,
        bag: Bag,
        race: RaceType
    ) {
        super(name, health, armor, abilityPoints, bag, race);
    }
}
