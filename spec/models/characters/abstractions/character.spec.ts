// tslint:disable-next-line
import 'reflect-metadata';
import { Bag, IItem, RaceType } from '../../../../src/models';
import { CharacterMock } from './mocks/character.mock';

describe('Character', () => {
    let mockBag: jest.Mock<Bag>;

    beforeEach(() => {
        mockBag = jest.fn<Bag>().mockImplementation(() => ({
            capacity: 100,
            items: []
        }));
    });

    describe('constructor should', () => {
        it('correctly assign passed values', () => {
            // Arrange
            const name: string = 'John';
            const health: number = 100;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;

            // Act
            const newCharacter: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);

            // Assert
            expect(newCharacter.name).toBe(name);
            expect(newCharacter.health).toBe(health);
            expect(newCharacter.armor).toBe(armor);
            expect(newCharacter.bag).toBe(bag);
            expect(newCharacter.race).toBe(race);
        });

        it('throw when passed name is 0 symbols long or space.', () => {
            // Arrange
            const name: string = ' ';
            const health: number = 100;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;

            // Act & Assert
            expect(() => (new CharacterMock(name, health, armor, abilityPoints, bag, race))).toThrow('Name cannot be null or empty.');
        });
    });

    describe('takeDamage should', () => {
        it('throw when character is not alive.', () => {
            // Arrange
            const name: string = 'John';
            const health: number = 100;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;
            const hitPoints: number = 40;

            // Act
            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
            character.isAlive = false;

            // Assert
            expect(() => character.takeDamage(hitPoints)).toThrow('Must be alive to perform this action.');
        });

        it('reduce the amount of character armor points with the corresponding number of hit points after taking damage.', () => {
            // Arrange
            const name: string = 'John';
            const health: number = 100;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;
            const hitPoints: number = 40;
            const armorAfterTakingDamage: number = armor - hitPoints;

            // Act
            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
            character.takeDamage(hitPoints);

            // Assert
            expect(character.armor).toBe(armorAfterTakingDamage);
        });

        it(`reduce the character health points if the amount of hit points is larger than character's armor points.`, () => {
            // Arrange
            const name: string = 'John';
            const health: number = 100;
            const armor: number = 10;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;
            const hitPoints: number = 40;
            const healthAfterTakingDamage: number = health - Math.abs(armor - hitPoints);

            // Act
            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
            character.takeDamage(hitPoints);

            // Assert
            expect(character.health).toBe(healthAfterTakingDamage);
        });

        it(`change the state of character from alive to dead if the character has no armor points
           and the amount of hit points equals the amount of character's health points.`,
           () => {
                // Arrange
                const name: string = 'John';
                const health: number = 40;
                const armor: number = 0;
                const abilityPoints: number = 40;
                const bag: Bag = new mockBag();
                const race: RaceType = RaceType.Alien;
                const hitPoints: number = 40;

                // Act
                const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
                character.takeDamage(hitPoints);

                // Assert
                expect(character.isAlive).toBeFalsy();
            });

        it(`change the state of character from alive to dead if the character has no armor points
           and the amount of hit points is larger than the amount of character's health points.`,
           () => {
                // Arrange
                const name: string = 'John';
                const health: number = 30;
                const armor: number = 0;
                const abilityPoints: number = 40;
                const bag: Bag = new mockBag();
                const race: RaceType = RaceType.Alien;
                const hitPoints: number = 40;

                // Act
                const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
                character.takeDamage(hitPoints);

                // Assert
                expect(character.isAlive).toBeFalsy();
            });
    });

    describe('rest should', () => {
        it('throw when character is not alive.', () => {
            // Arrange
            const name: string = 'John';
            const health: number = 100;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;

            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
            character.isAlive = false;

            // Act & Assert
            expect(() => (character.rest())).toThrow('Must be alive to perform this action.');
        });

        it(`increase the amount of health points by 20 percent of character's base health points.`, () => {
            // Arrange
            const name: string = 'John';
            const health: number = 60;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;
            const baseHealth: number = 100;
            const healthPointsAfterRest: number = health + baseHealth * 0.2;

            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);

            // Act
            character.rest();

            // Assert
            expect(character.health).toBe(healthPointsAfterRest);
        });

        it(`increase the amount of health points by 20 percent and if that exceeds the amount of base health points,
           restore all base health points`,
           () => {
                // Arrange
                const name: string = 'John';
                const health: number = 90;
                const armor: number = 50;
                const abilityPoints: number = 40;
                const bag: Bag = new mockBag();
                const race: RaceType = RaceType.Alien;
                const baseHealth: number = 100;

                const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);

                // Act
                character.rest();

                // Assert
                expect(character.health).toBe(baseHealth);
            });
    });

    describe('useItem should', () => {
        let mockHealthPotion: jest.Mock<IItem>;

        beforeEach(() => {
            mockHealthPotion = jest.fn<IItem>().mockImplementation(() => ({
                hitPointsRestored: 20,
                weight: 5,
                affectCharacter: jest.fn().mockReturnValue(30)
            }));
        });

        it('throw if the character is not alive.', () => {
            // Arrange
            const name: string = 'John';
            const health: number = 10;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;

            const newHealthPotion: IItem = new mockHealthPotion();

            bag.items.push(newHealthPotion);

            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);
            character.isAlive = false;

            // Act & Assert
            expect(() => (character.useItem(newHealthPotion))).toThrow('Must be alive to perform this action.');
        });

        it(`call "affectCharacter" command in class Item once.`, () => {
            // Arrange
            const name: string = 'John';
            const health: number = 10;
            const armor: number = 50;
            const abilityPoints: number = 40;
            const bag: Bag = new mockBag();
            const race: RaceType = RaceType.Alien;

            const newHealthPotion: IItem = new mockHealthPotion();

            bag.items.push(newHealthPotion);

            const character: CharacterMock = new CharacterMock(name, health, armor, abilityPoints, bag, race);

            const spy: jest.SpyInstance = jest.spyOn(newHealthPotion, 'affectCharacter');

            // Act
            character.useItem(newHealthPotion);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });
    });
});
