// tslint:disable-next-line
import 'reflect-metadata';
import { AddItemToPool, ICommand } from '../../src/commands';
import { IItemFactory } from '../../src/core';
import { IRepository } from '../../src/data';
import { IItem } from '../../src/models';

describe('AddItemToPool', () => {
    describe('"execute" method should', () => {
        let mockItemFactory: jest.Mock<IItemFactory>;
        let mockRepository: jest.Mock<IRepository>;

        beforeAll(() => {
            mockItemFactory = jest.fn<IItemFactory>().mockImplementation(() => ({
                createItem: jest.fn()
            }));

            mockRepository = jest.fn<IRepository>().mockImplementation(() => ({
                itemPool: []
            }));
        });

        it('call the itemFactory\'s "createItem" method once', () => {
            // Arrange
            const itemFactory: IItemFactory = new mockItemFactory();
            const repository: IRepository = new mockRepository();

            const addItemToPoolCommand: ICommand = new AddItemToPool(itemFactory, repository);
            const spy: jest.SpyInstance = jest.spyOn(itemFactory, 'createItem');

            // Act
            addItemToPoolCommand.execute(['HealthPotion']);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('call the itemFactory\'s "createItem" method with parameter', () => {
            // Arrange
            const itemFactory: IItemFactory = new mockItemFactory();
            const repository: IRepository = new mockRepository();

            const addItemToPoolCommand: ICommand = new AddItemToPool(itemFactory, repository);
            const spy: jest.SpyInstance = jest.spyOn(itemFactory, 'createItem');

            // Act
            addItemToPoolCommand.execute(['HealthPotion']);

            // Assert
            expect(spy).toBeCalledWith('HealthPotion');
        });

        it('call the repository.itemPool\'s "push" method once', () => {
            // Arrange
            const itemFactory: IItemFactory = new mockItemFactory();
            const repository: IRepository = new mockRepository();

            const addItemToPoolCommand: ICommand = new AddItemToPool(itemFactory, repository);
            const spy: jest.SpyInstance = jest.spyOn(repository.itemPool, 'push');

            // Act
            addItemToPoolCommand.execute(['HealthPotion']);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('correctly add an item to the array of items in the pool', () => {
            // Arrange
            const mockItem: jest.Mock<IItem> = jest.fn<IItem>().mockImplementation();

            const repository: IRepository = new mockRepository();
            const item: IItem = new mockItem();

            // Act
            repository.itemPool.push(item);

            // Assert
            expect(repository.itemPool.length).toBe(1);
        });

        it('return a correct message', () => {
            // Arrange
            const repository: IRepository = new mockRepository();
            const itemFactory: IItemFactory = new mockItemFactory();

            const addItemToPoolCommand: ICommand = new AddItemToPool(itemFactory, repository);

            // Act
            const itemName: string = 'HealthPotion';
            const message: string = addItemToPoolCommand.execute([itemName]);

            // Assert
            expect(message).toBe(`${itemName} added to pool.`);
        });
    });
});
