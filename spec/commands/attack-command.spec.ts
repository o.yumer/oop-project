// tslint:disable-next-line
import 'reflect-metadata';
import { Attack, ICommand } from '../../src/commands';
import { IRepository } from '../../src/data';
import { Healer, ICharacter, RaceType, Warrior } from '../../src/models';

describe('Attack', () => {
    describe('"execute" method should', () => {
        let mockRepository: jest.Mock<IRepository>;
        let mockCharacter: jest.Mock<ICharacter>;

        beforeAll(() => {
            mockRepository = jest.fn<IRepository>().mockImplementation(() => ({
                party: []
            }));

            mockCharacter = jest.fn<ICharacter>().mockImplementation(() => ({
                name: 'Pesho',
                attack: jest.fn()
            }));
        });

        it('throw error when the attacker not found', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const receiver: ICharacter = new mockCharacter();

            repository.party.push(receiver);

            // Act & Assert
            const attackerName: string = 'Tosho';
            expect(() => attackCommand.execute([attackerName, 'Pesho'])).toThrow(`Attacker ${attackerName} not found.`);
        });

        it('throw error when the receiver not found', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const attacker: ICharacter = new mockCharacter();

            repository.party.push(attacker);

            // Act & Assert
            const receiverName: string = 'Tosho';
            expect(() => attackCommand.execute(['Pesho', receiverName])).toThrow(`Receiver ${receiverName} not found.`);
        });

        it('throw error when the attacker isn\'t instance of Warrior', () => {
            // Arrange
            const attackerName: string = 'Gosho';
            const mockHealer: jest.Mock<Healer> = jest.fn<Healer>().mockImplementation(() => ({
                name: attackerName
            }));

            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const healer: Healer = new mockHealer();
            const receiver: ICharacter = new mockCharacter();

            repository.party.push(healer);
            repository.party.push(receiver);

            // Act & Assert
            expect(() => attackCommand.execute([attackerName, 'Pesho'])).toThrow(`${attackerName} cannot attack.`);
        });

        it('call the attacker\'s "attack" method once', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const attacker: Warrior = new Warrior('Pesho', RaceType.Alien);
            const receiver: ICharacter = new Warrior('Gosho', RaceType.Terrestrial);

            repository.party.push(attacker);
            repository.party.push(receiver);

            const spy: jest.SpyInstance = jest.spyOn(attacker, 'attack');

            // Act
            attackCommand.execute(['Pesho', 'Gosho']);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('call the attacker\'s "attack" method with parameter', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const attacker: Warrior = new Warrior('Pesho', RaceType.Alien);
            const receiver: ICharacter = new Warrior('Gosho', RaceType.Terrestrial);

            repository.party.push(attacker);
            repository.party.push(receiver);

            const spy: jest.SpyInstance = jest.spyOn(attacker, 'attack');

            // Act
            attackCommand.execute(['Pesho', 'Gosho']);

            // Assert
            expect(spy).toBeCalledWith(receiver);
        });

        it('return a correct message when the receiver is alive', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const attacker: Warrior = new Warrior('Pesho', RaceType.Alien);
            const receiver: ICharacter = new Warrior('Gosho', RaceType.Terrestrial);

            repository.party.push(attacker);
            repository.party.push(receiver);

            let result: string = `${attacker.name} attacks ${receiver.name} with force ${attacker.abilityPoints}! `;
            result += `${receiver.name} has ${receiver.health}/${receiver.baseHealth} health points and `;
            result += `${receiver.armor - 40}/${receiver.baseArmor} armor points left after defence!`;

            // Act
            const message: string = attackCommand.execute(['Pesho', 'Gosho']);

            // Assert
            expect(message).toBe(result);
        });

        it('return a correct message when the receiver is dead', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const attackCommand: ICommand = new Attack(repository);

            const attacker: Warrior = new Warrior('Pesho', RaceType.Alien);
            const receiver: ICharacter = new Warrior('Gosho', RaceType.Terrestrial);
            receiver.health = 10;
            receiver.armor = 0;

            repository.party.push(attacker);
            repository.party.push(receiver);

            let result: string = `${attacker.name} attacks ${receiver.name} with force ${attacker.abilityPoints}! `;
            result += `${receiver.name} has ${receiver.health - 10}/${receiver.baseHealth} health points and `;
            result += `${receiver.armor}/${receiver.baseArmor} armor points left after defence!`;
            result += `\r\n${receiver.name} is dead!`;

            // Act
            const message: string = attackCommand.execute(['Pesho', 'Gosho']);

            // Assert
            expect(message).toBe(result);
        });
    });
});
