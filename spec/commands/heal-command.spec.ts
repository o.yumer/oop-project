// tslint:disable-next-line
import 'reflect-metadata';
import { Heal } from '../../src/commands';
import { IRepository } from '../../src/data';
import { Healer, ICharacter, RaceType, Warrior } from '../../src/models';
import { ICommand } from './../../src/commands/contracts/command';

describe('Heal', () => {
    describe('"execute" method should', () => {
        let mockRepository: jest.Mock<IRepository>;
        let mockCharacterHealer: jest.Mock<Healer>;
        let mockCharacterReceiver: jest.Mock<ICharacter>;
        let mockCharacterWarrior: jest.Mock<Warrior>;

        beforeEach(() => {
            mockRepository = jest.fn<IRepository>().mockImplementation(() => ({
                party: []
            }));

            mockCharacterHealer = jest.fn<Healer>().mockImplementation(() => ({
                name: 'Doctor',
                heal: jest.fn()
            }));

            mockCharacterReceiver = jest.fn<ICharacter>().mockImplementation(() => ({
                name: 'Patient'
            }));

            mockCharacterWarrior = jest.fn<Warrior>().mockImplementation(() => ({
                name: 'Warrior'
            }));
        });

        it('throw when healer is not found.', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const patient: ICharacter = new mockCharacterReceiver();

            const healCommand: ICommand = new Heal(repository);

            repository.party.push(patient);

            // Act & Assert
            expect(() => healCommand.execute(['Doctor', 'Patient'])).toThrow(`Healer Doctor not found.`);
        });

        it('throw when receiver is not found.', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const healer: Healer = new mockCharacterHealer();

            const healCommand: ICommand = new Heal(repository);

            repository.party.push(healer);

            // Act & Assert
            expect(() => healCommand.execute(['Doctor', 'Patient'])).toThrow(`Receiver Patient not found.`);
        });

        it('throw when healer is not instance of class Healer', () => {
            // Arrange
            const repository: IRepository = new mockRepository();

            const healer: Warrior = new mockCharacterWarrior();
            const patient: ICharacter = new mockCharacterReceiver();

            const healCommand: ICommand = new Heal(repository);

            repository.party.push(healer);
            repository.party.push(patient);

            // Act & Assert
            expect(() => healCommand.execute(['Warrior', 'Patient'])).toThrow(`Warrior cannot heal.`);
        });

        it(`call healer's "heal" method once`, () => {
             // Arrange
             const repository: IRepository = new mockRepository();

             const healer: Healer = new Healer('Doctor', RaceType.Alien);
             const patient: ICharacter = new Warrior('Patient', RaceType.Alien);

             const healCommand: ICommand = new Heal(repository);

             repository.party.push(healer);
             repository.party.push(patient);

             const spy: jest.SpyInstance = jest.spyOn(healer, 'heal');

             // Act
             healCommand.execute(['Doctor', 'Patient']);

             // Assert
             expect(spy).toBeCalledTimes(1);
            });
    });
});
