// tslint:disable-next-line
import 'reflect-metadata';
import { Engine, ICommandProcessor, IReader, IWriter } from '../../src/core';

describe('Engine', () => {
    describe('start should', () => {
        let mockReader: jest.Mock<IReader>;
        let mockWriter: jest.Mock<IWriter>;
        let mockCommandProcessor: jest.Mock<ICommandProcessor>;

        let spy: jest.SpyInstance;

        beforeEach(() => {
            mockReader = jest.fn<IReader>().mockImplementation(() => ({
                read: jest.fn().mockReturnValue(Promise.resolve(['command']))
            }));

            mockWriter = jest.fn<IWriter>().mockImplementation(() => ({
                write: jest.fn()
            }));

            mockCommandProcessor = jest.fn<ICommandProcessor>().mockImplementation(() => ({
                processCommand: jest.fn().mockReturnValue('command result')
            }));
        });

        afterEach(() => {
            spy.mockRestore();
        });

        it('call the reader\'s "read" method once', async () => {
            // Arrange
            const reader: IReader = new mockReader();
            const writer: IWriter = new mockWriter();
            const commandProcessor: ICommandProcessor = new mockCommandProcessor();

            spy = jest.spyOn(reader, 'read');

            const engine: Engine = new Engine(commandProcessor, reader, writer);

            // Act
            await engine.start();

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('call the writer\'s "write" method once with correct parameter when the command is processes correctly', async () => {
            // Arrange
            const reader: IReader = new mockReader();
            const writer: IWriter = new mockWriter();
            const commandProcessor: ICommandProcessor = new mockCommandProcessor();

            spy = jest.spyOn(writer, 'write');

            const engine: Engine = new Engine(commandProcessor, reader, writer);

            // Act
            await engine.start();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('command result');
        });

        it('call the writer\'s "write" method once with correct error message when the commandProcessor throws', async () => {
            // Arrange
            mockCommandProcessor = jest.fn<ICommandProcessor>().mockImplementation(() => ({
                processCommand: jest.fn().mockImplementation(() => {
                    throw new Error('error message');
                })
            }));

            const reader: IReader = new mockReader();
            const writer: IWriter = new mockWriter();
            const commandProcessor: ICommandProcessor = new mockCommandProcessor();

            spy = jest.spyOn(writer, 'write');

            const engine: Engine = new Engine(commandProcessor, reader, writer);

            // Act
            await engine.start();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('error message');
        });

        it('call the commandProcessor\'s "processCommand" method once with correct command.', async () => {
            // Arrange
            const reader: IReader = new mockReader();
            const writer: IWriter = new mockWriter();
            const commandProcessor: ICommandProcessor = new mockCommandProcessor();

            spy = jest.spyOn(commandProcessor, 'processCommand');

            const engine: Engine = new Engine(commandProcessor, reader, writer);

            // Act
            await engine.start();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('command');
        });

        it('call the reader, writer and commandProcessor exact number of times with correct parameters', async () => {
            // Arrange
            mockReader = jest.fn<IReader>().mockImplementation(() => ({
                read: jest.fn().mockReturnValue(Promise.resolve(['command1', 'command2', 'command3']))
            }));

            mockCommandProcessor = jest.fn<ICommandProcessor>().mockImplementation(() => ({
                processCommand: jest.fn().mockImplementation((command: string) => {
                    if (command === 'command2') {
                        throw new Error('error message');
                    }

                    return 'command result';
                })
            }));

            const reader: IReader = new mockReader();
            const writer: IWriter = new mockWriter();
            const commandProcessor: ICommandProcessor = new mockCommandProcessor();

            const readerSpy: jest.SpyInstance = jest.spyOn(reader, 'read');
            const writerSpy: jest.SpyInstance = jest.spyOn(writer, 'write');
            const commandProcessorSpy: jest.SpyInstance = jest.spyOn(commandProcessor, 'processCommand');

            const engine: Engine = new Engine(commandProcessor, reader, writer);

            const executionResult: string = ['command result', 'error message', 'command result'].join('\n');

            // Act
            await engine.start();

            // Assert
            expect(readerSpy).toBeCalledTimes(1);

            expect(commandProcessorSpy).toBeCalledTimes(3);
            expect(commandProcessorSpy).toBeCalledWith('command1');
            expect(commandProcessorSpy).toBeCalledWith('command2');
            expect(commandProcessorSpy).toBeCalledWith('command3');

            expect(writerSpy).toBeCalledTimes(1);
            expect(writerSpy).toBeCalledWith(executionResult);

            readerSpy.mockRestore();
            commandProcessorSpy.mockRestore();
            writerSpy.mockRestore();
        });
    });
});
