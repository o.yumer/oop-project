// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { container } from './common/ioc.config';
import { TYPES } from './common/Types';
import { IEngine } from './core/contracts/engine';

const engine: IEngine = container.get<IEngine>(TYPES.engine);
engine.start();
