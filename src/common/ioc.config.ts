import { Container, interfaces } from 'inversify';
import { AddItemToPool,
         Attack,
         EndTurn,
         GetStats,
         GiveCharacterItem,
         Heal,
         ICommand,
         JoinParty,
         PickUpItem,
         UseItem,
         UseItemOn
} from '../commands';
import { CharacterFactory,
         CommandProcessor,
         ConsoleWriter,
         ContainerCommandFactory,
         DataFormatter,
         Engine,
         FileReader,
         ICharacterFactory,
         ICommandFactory,
         ICommandProcessor,
         IDataFormatter,
         IEngine,
         IItemFactory,
         IReader,
         ItemFactory,
         IWriter
} from '../core';

import { IRepository,
         Repository
} from '../data';
import { TYPES } from './Types';

const container: Container = new Container();

container.bind<IEngine>(TYPES.engine).to(Engine);
container.bind<IRepository>(TYPES.repository).to(Repository).inSingletonScope();
container.bind<IReader>(TYPES.reader).to(FileReader);
container.bind<IWriter>(TYPES.writer).to(ConsoleWriter);
container.bind<ICommandProcessor>(TYPES.commandProcessor).to(CommandProcessor);
container.bind<IDataFormatter>(TYPES.dataFormatter).to(DataFormatter);
container.bind<IItemFactory>(TYPES.itemFactory).to(ItemFactory);
container.bind<ICharacterFactory>(TYPES.characterFactory).to(CharacterFactory);

container.bind<ICommand>(TYPES.additemtopool).to(AddItemToPool);
container.bind<ICommand>(TYPES.attack).to(Attack);
container.bind<ICommand>(TYPES.endturn).to(EndTurn);
container.bind<ICommand>(TYPES.getstats).to(GetStats);
container.bind<ICommand>(TYPES.givecharacteritem).to(GiveCharacterItem);
container.bind<ICommand>(TYPES.heal).to(Heal);
container.bind<ICommand>(TYPES.joinparty).to(JoinParty);
container.bind<ICommand>(TYPES.pickupitem).to(PickUpItem);
container.bind<ICommand>(TYPES.useitem).to(UseItem);
container.bind<ICommand>(TYPES.useitemon).to(UseItemOn);

container
    .bind<interfaces.Factory<ICommand>>(TYPES.containerCommandFactory)
    .toFactory<ICommand>((context: interfaces.Context) =>
        (commandName: string): ICommand =>
            context.container.get<ICommand>(TYPES[commandName]));

container.bind<ICommandFactory>(TYPES.commandFactory).to(ContainerCommandFactory);

export { container };
