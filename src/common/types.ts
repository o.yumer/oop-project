import { IType } from './contracts/types';

export const TYPES: IType = {
    reader: Symbol.for('reader'),
    writer: Symbol.for('writer'),
    engine: Symbol.for('engine'),
    commandProcessor: Symbol.for('commandProcessor'),
    dataFormatter: Symbol.for('dataFormatter'),
    repository: Symbol.for('repository'),
    containerCommandFactory: Symbol.for('containerCommandFactory'),
    containerCharacterFactory: Symbol.for('containerCharacterFactory'),
    commandFactory: Symbol.for('commandFactory'),
    characterFactory: Symbol.for('characterFactory'),
    itemFactory: Symbol.for('itemFactory'),
    additemtopool: Symbol.for('additemtopool'),
    attack: Symbol.for('attack'),
    endturn: Symbol.for('endturn'),
    getstats: Symbol.for('getstats'),
    givecharacteritem: Symbol.for('givecharacteritem'),
    heal: Symbol.for('heal'),
    joinparty: Symbol.for('joinparty'),
    pickupitem: Symbol.for('pickupitem'),
    useitem: Symbol.for('useitem'),
    useitemon: Symbol.for('useitemon'),
    warrior: Symbol.for('warrior'),
    healer: Symbol.for('healer')
};
