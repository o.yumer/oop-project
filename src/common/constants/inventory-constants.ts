// tslint:disable-next-line:no-unnecessary-class
export class InventoryConstants {
    public static BACKPACK_CAPACITY: number = 100;

    public static SATCHEL_CAPACITY: number = 20;

    public static GET_BAG_IS_FULL_ERROR_MESSAGE(): string {
        return 'Bag is full!';
    }

    public static GET_BAG_IS_EMPTY_ERROR_MESSAGE(): string {
        return 'Bag is empty!';
    }

    public static GET_NO_SUCH_ITEM_IN_BAG_ERROR_MESSAGE(itemName: string): string {
        return `No item with name ${itemName} in bag!`;
    }
}
