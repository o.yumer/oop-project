// tslint:disable-next-line:no-unnecessary-class
export class ItemsConstants {
    public static ARMOR_REPAIR_KIT_WEIGHT: number = 10;

    public static HEALTH_POTION_WEIGHT: number = 5;

    public static POISON_POTION_WEIGHT: number = 5;

    public static GET_NO_SUCH_ITEM_ERROR_MESSAGE(itemName: string): string {
        return `There is no item of type ${itemName}.`;
    }
}
