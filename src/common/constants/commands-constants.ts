import { ICharacter,
         IItem
} from '../../models';

// tslint:disable-next-line:no-unnecessary-class
export class CommandsConstants {
    // Success messages
    public static GET_JOIN_PARTY_SUCCESS_MESSAGE(name: string): string {
        return `${name} joined the party!`;
    }

    public static GET_ADD_ITEM_TO_POOL_SUCCESS_MESSAGE(itemName: string): string {
        return `${itemName} added to pool.`;
    }

    public static GET_PICK_UP_ITEM_SUCCESS_MESSAGE(characterName: string, item: IItem): string {
        return `${characterName} picked up ${item.constructor.name}.`;
    }

    public static GET_USE_ITEM_SUCCESS_MESSAGE(characterName: string, itemName: string): string {
        return `${characterName} used ${itemName}.`;
    }

    public static GET_USE_ITEM_ON_SUCCESS_MESSAGE(giverName: string, itemName: string, receiverName: string): string {
        return `${giverName} used ${itemName} on ${receiverName}.`;
    }

    public static GET_GIVE_CHARACTER_ITEM_SUCCESS_MESSAGE(giverName: string, receiverName: string, itemName: string): string {
        return `${giverName} gave ${receiverName} ${itemName}.`;
    }

    public static GET_ATTACK_SUCCESS_MESSAGE(attacker: ICharacter, receiver: ICharacter): string {
        let msg: string = `${attacker.name} attacks ${receiver.name} with force ${attacker.abilityPoints}! `;
        msg += `${receiver.name} has ${receiver.health}/${receiver.baseHealth} health points and `;
        msg += `${receiver.armor}/${receiver.baseArmor} armor points left after defence!`;

        return msg;
    }

    public static GET_KILL_SUCCESS_MESSAGE(receiver: ICharacter): string {
        return `${receiver.name} is dead!`;
    }

    public static GET_REST_SUCCESS_MESSAGE(character: ICharacter, healthBeforeRest: number, healthAfterRest: number): string {
        return `${character.name} rests (${healthBeforeRest} => ${healthAfterRest})\r\n`;
    }

    public static GET_FINAL_STATS_MESSAGE(): string {
        return 'Final stats:';
    }

    public static GET_STATS_SUCCESS_MESSAGE(c: ICharacter): string {
        return `${c.name} - HP: ${c.health}/${c.baseHealth}, AP: ${c.armor}/${c.baseArmor}, Status: ${c.isAlive ? 'Alive' : 'Dead'}`;
    }

    public static GET_HEAL_SUCCESS_MESSAGE(healer: ICharacter, receiver: ICharacter): string {
        return `${healer.name} heals ${receiver.name} for ${healer.abilityPoints}! ${receiver.name} has ${receiver.health} health now.`;
    }

    // Error messages
    public static GET_ATTACKER_NOT_FOUND_ERROR_MESSAGE(attackerName: string): string {
        return `Attacker ${attackerName} not found.`;
    }

    public static GET_RECEIVER_NOT_FOUND_ERROR_MESSAGE(receiverName: string): string {
        return `Receiver ${receiverName} not found.`;
    }

    public static GET_GIVER_NOT_FOUND_ERROR_MESSAGE(giverName: string): string {
        return `Giver ${giverName} not found.`;
    }

    public static GET_HEALER_NOT_FOUND_ERROR_MESSAGE(healerName: string): string {
        return `Healer ${healerName} not found.`;
    }

    public static GET_CANNOT_ATTACK_ERROR_MESSAGE(attackerName: string): string {
        return `${attackerName} cannot attack.`;
    }

    public static GET_CANNOT_HEAL_ERROR_MESSAGE(healerName: string): string {
        return `${healerName} cannot heal.`;
    }

    public static GET_CHARACTER_NOT_FOUND_ERROR_MESSAGE(characterName: string): string {
        return `Character with name ${characterName} not found.`;
    }

    public static GET_NO_ITEMS_IN_POOL_ERROR_MESSAGE(): string {
        return `No items left in pool!`;
    }
}
