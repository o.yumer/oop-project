// tslint:disable-next-line:no-unnecessary-class
export class CharacterConstants {
    public static HEALER_BASE_HEALTH_POINTS: number = 50;

    public static HEALER_BASE_ARMOR_POINTS: number = 25;

    public static HEALER_ABILITY_POINTS: number = 40;

    public static WARRIOR_BASE_HEALTH_POINTS: number = 100;

    public static WARRIOR_BASE_ARMOR_POINTS: number = 50;

    public static WARRIOR_ABILITY_POINTS: number = 40;

    // Error messages
    public static GET_NAME_CANNOT_BE_NULL_ERROR_MESSAGE(): string {
        return 'Name cannot be null or empty.';
    }

     public static GET_NOT_ALIVE_WARNING_ERROR_MESSAGE(): string {
        return 'Must be alive to perform this action.';
    }

    public static GET_CANNOT_HEAL_ENEMIES_ERROR_MESSAGE(): string {
        return 'A healer cannot heal enemy characters!';
    }

    public static GET_CANNOT_ATTACK_SELF_ERROR_MESSAGE(): string {
        return 'Cannot attack self!';
    }

    public static GET_CANNOT_ATTACK_CHARACTER_FROM_SAME_RACE_ERROR_MESSAGE(): string {
        return 'Cannot attack a character from the same race!';
    }

    public static GET_INVALID_RACE_ERROR_MESSAGE(race: string): string {
        return `Invalid race ${race}`;
    }

    public static GET_INVALID_CHARACTER_TYPE_ERROR_MESSAGE(characterType: string): string {
        return `Invalid character type ${characterType}`;
    }
}
