export interface IType {
    reader: symbol;
    writer: symbol;
    engine: symbol;
    commandProcessor: symbol;
    dataFormatter: symbol;
    repository: symbol;
    containerCommandFactory: symbol;
    containerCharacterFactory: symbol;
    commandFactory: symbol;
    characterFactory: symbol;
    itemFactory: symbol;
    [key: string]: symbol;
    additemtopool: symbol;
    attack: symbol;
    endturn: symbol;
    getstats: symbol;
    givecharacteritem: symbol;
    heal: symbol;
    joinparty: symbol;
    pickupitem: symbol;
    useitem: symbol;
    useitemon: symbol;
    warrior: symbol;
    healer: symbol;
}
