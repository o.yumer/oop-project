import { InventoryConstants } from './../../common/constants';
import { Bag } from './bag';

export class Backpack extends Bag {
    public constructor() {
        super(InventoryConstants.BACKPACK_CAPACITY);
    }
}
