import { IItem } from './../../items/contracts';

export interface IBag {
    capacity: number;

    items: IItem[];
}
