import { InventoryConstants } from '../../common/constants';
import { Bag } from './bag';

export class Satchel extends Bag {
    public constructor() {
        super(InventoryConstants.SATCHEL_CAPACITY);
    }
}
