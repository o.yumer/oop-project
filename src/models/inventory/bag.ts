import { InventoryConstants, ItemsConstants } from '../../common/constants';
import { IItem } from '../items';
import { IBag } from './contracts';

export abstract class Bag implements IBag {
    private readonly _capacity: number;
    private readonly _items: IItem[];
    private _load: number;

    protected constructor(capacity: number) {
        this._capacity = capacity;
        this._items = [];
    }

    public get capacity(): number {
        return this._capacity;
    }

    public get items(): IItem[] {
        return this._items;
    }

    private get load(): number {
        this.items.forEach((element: IItem) => {
            this._load += element.weight;
        });

        return this._load || 0;
    }

    public addItem(item: IItem): void {
        if (this.load + item.weight > this.capacity) {
            throw new Error(InventoryConstants.GET_BAG_IS_FULL_ERROR_MESSAGE());
        }

        this.items.push(item);
    }

    public getItem(itemName: string): IItem {
        this.itemExistsValidation(itemName);

        const item: IItem | undefined = this.items.find((i: IItem) => i.constructor.name === itemName);

        if (!item) {
            throw new Error(ItemsConstants.GET_NO_SUCH_ITEM_ERROR_MESSAGE(itemName));
        }

        this.items.splice(this.items.indexOf(item), 1);

        return item;
    }

    private itemExistsValidation(itemName: string): void {
        if (!this.items.length) {
            throw new Error(InventoryConstants.GET_BAG_IS_EMPTY_ERROR_MESSAGE());
        }

        const itemExists: boolean = this.items.some((i: IItem) => i.constructor.name === itemName);

        if (!itemExists) {
            throw new Error(InventoryConstants.GET_NO_SUCH_ITEM_IN_BAG_ERROR_MESSAGE(itemName));
        }
    }
}
