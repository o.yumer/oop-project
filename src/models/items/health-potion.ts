import { ICharacter } from '../characters/contracts/character';
import { ItemsConstants } from './../../common/constants';
import { Item } from './item';

export class HealthPotion extends Item {
    private readonly hitPointsRestored: number = 20;

    public constructor() {
        super(ItemsConstants.ARMOR_REPAIR_KIT_WEIGHT);
    }

    public affectCharacter(character: ICharacter): void {
        super.affectCharacter(character);

        if (character.health + this.hitPointsRestored > character.baseHealth) {
            character.health = character.baseHealth;
        } else {
            character.health += this.hitPointsRestored;
        }
    }
}
