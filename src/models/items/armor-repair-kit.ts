import { ICharacter } from '../characters';
import { ItemsConstants } from './../../common/constants';
import { Item } from './item';

export class ArmorRepairKit extends Item {
    public constructor() {
        super(ItemsConstants.ARMOR_REPAIR_KIT_WEIGHT);
    }

    public affectCharacter(character: ICharacter): void {
        super.affectCharacter(character);
        character.armor = character.baseArmor;
    }
}
