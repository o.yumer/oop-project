import { ICharacter } from '../characters/contracts/character';
import { ItemsConstants } from './../../common/constants';
import { Item } from './item';

export class PoisonPotion extends Item {
    private readonly hitPointsDamaged: number = 20;

    public constructor() {
        super(ItemsConstants.POISON_POTION_WEIGHT);
    }

    public affectCharacter(character: ICharacter): void {
        super.affectCharacter(character);

        character.health = Math.max(0, character.health - this.hitPointsDamaged);

        if (character.health <= 0) {
            character.isAlive = false;
        }
    }
}
