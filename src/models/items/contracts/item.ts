import { ICharacter } from '../../characters/contracts/character';

export interface IItem {
    weight: number;

    affectCharacter(character: ICharacter): void;
}
