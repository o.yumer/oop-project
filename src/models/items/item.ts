import { ICharacter } from '../characters/contracts/character';
import { CharacterConstants } from './../../common/constants';
import { IItem } from './contracts/item';

export abstract class Item implements IItem {
    private readonly _weight: number;

    public constructor(weight: number) {
        this._weight = weight;
    }

    public get weight(): number {
        return this._weight;
    }

    public affectCharacter(character: ICharacter): void {
        if (!character.isAlive) {
            throw new Error(CharacterConstants.GET_NOT_ALIVE_WARNING_ERROR_MESSAGE());
        }
    }
}
