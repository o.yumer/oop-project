import { injectable } from 'inversify';
import { Bag } from '../inventory';
import { IItem } from '../items';
import { CharacterConstants } from './../../common/constants';
import { RaceType } from './common';
import { ICharacter } from './contracts';

@injectable()
export abstract class Character implements ICharacter {
    protected readonly _baseHealth: number;

    private readonly _name: string;
    private _health: number;
    private _armor: number;
    private readonly _baseArmor: number;
    private readonly _race: RaceType;
    private readonly _abilityPoints: number;
    private readonly _bag: Bag;
    private _isAlive: boolean = true;
    private readonly _restHealMultiplier: number = 0.2;

    protected constructor(
        name: string,
        health: number,
        armor: number,
        abilityPoints: number,
        bag: Bag,
        race: RaceType
    ) {
        if (name.length <= 0 || name === ' ') {
            throw new Error(CharacterConstants.GET_NAME_CANNOT_BE_NULL_ERROR_MESSAGE());
        }

        this._name = name;
        this._health = health;
        this._baseHealth = health;
        this._armor = armor;
        this._baseArmor = armor;
        this._abilityPoints = abilityPoints;
        this._bag = bag;
        this._race = race;
    }

    public get name(): string {
        return this._name;
    }

    public get baseHealth(): number {
        return this._baseHealth;
    }

    public get health(): number {
        return this._health;
    }

    public set health(newHealth: number) {
        this._health = newHealth;
    }

    public get baseArmor(): number {
        return this._baseArmor;
    }

    public get armor(): number {
        return this._armor;
    }

    public set armor(newArmor: number) {
        this._armor = newArmor;
    }

    public get abilityPoints(): number {
        return this._abilityPoints;
    }

    public get bag(): Bag {
        return this._bag;
    }

    public get isAlive(): boolean {
        return this._isAlive;
    }

    public set isAlive(newValue: boolean) {
        this._isAlive = newValue;
    }

    public get race(): RaceType {
        return this._race;
    }

    public takeDamage(hitPoints: number): void {
        this.isAliveValidate();

        const hitPointsLeftAfterArmorDamage: number = Math.max(0, hitPoints - this.armor);
        this.armor = Math.max(0, this.armor - hitPoints);
        this.health = Math.max(0, this.health - hitPointsLeftAfterArmorDamage);

        if (this.health <= 0) {
            this.isAlive = false;
        }
    }

    public rest(): void {
        this.isAliveValidate();

        if (this.health < this.baseHealth) {
            this.health = Math.min(this.health + (this.baseHealth * this._restHealMultiplier), this.baseHealth);
        }
    }

    public useItem(item: IItem): void {
        this.isAliveValidate();
        item.affectCharacter(this);
    }

    public useItemOn(item: IItem, character: ICharacter): void {
        this.isAliveValidate();
        character.isAliveValidate();
        item.affectCharacter(character);
    }

    public giveCharacterItem(item: IItem, character: ICharacter): void {
        this.isAliveValidate();
        character.isAliveValidate();
        character.bag.addItem(item);
    }

    public receiveItem(item: IItem): void {
        this.isAliveValidate();
        this.bag.addItem(item);
    }

    public isAliveValidate(): void {
        if (this.isAlive === false) {
            throw new Error(CharacterConstants.GET_NOT_ALIVE_WARNING_ERROR_MESSAGE());
        }
    }
}
