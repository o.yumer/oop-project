import { ICharacter } from './character';

export interface IHealer {
    heal(character: ICharacter): void;
}
