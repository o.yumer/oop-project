import { ICharacter } from './character';

export interface IAttacker {
    attack(character: ICharacter): void;
}
