import { Bag } from '../../inventory';
import { IItem } from '../../items';
import { RaceType } from '../common';

export interface ICharacter {
    name: string;

    baseHealth: number;

    health: number;

    baseArmor: number;

    armor: number;

    abilityPoints: number;

    bag: Bag;

    race: RaceType;

    isAlive: boolean;

    takeDamage(hitPoints: number): void;

    rest(): void;

    useItem(item: IItem): void;

    useItemOn(item: IItem, character: ICharacter): void;

    giveCharacterItem(item: IItem, character: ICharacter): void;

    receiveItem(item: IItem): void;

    isAliveValidate(): void;
}
