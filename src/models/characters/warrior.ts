import { injectable } from 'inversify';
import { CharacterConstants } from './../../common/constants/characters-constants';
import { Satchel } from './../inventory/satchel';
import { Character } from './character';
import { RaceType } from './common';
import {
    IAttacker,
    ICharacter
} from './contracts';

@injectable()
export class Warrior extends Character implements IAttacker {
    public constructor(name: string, race: RaceType) {
        super(
            name,
            CharacterConstants.WARRIOR_BASE_HEALTH_POINTS,
            CharacterConstants.WARRIOR_BASE_ARMOR_POINTS,
            CharacterConstants.WARRIOR_ABILITY_POINTS,
            new Satchel(),
            race
        );
    }

    public attack(character: ICharacter): void {
        this.isAliveValidate();
        character.isAliveValidate();

        if (character === this) {
            throw new Error(CharacterConstants.GET_CANNOT_ATTACK_SELF_ERROR_MESSAGE());
        }

        if (character.race === this.race) {
            throw new Error(CharacterConstants.GET_CANNOT_ATTACK_CHARACTER_FROM_SAME_RACE_ERROR_MESSAGE());
        }

        character.takeDamage(this.abilityPoints);
    }
}
