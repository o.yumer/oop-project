import { injectable } from 'inversify';
import { CharacterConstants } from './../../common/constants/characters-constants';
import { Backpack } from './../inventory/backpack';
import { Character } from './character';
import { RaceType } from './common';
import {
    ICharacter,
    IHealer
} from './contracts';

@injectable()
export class Healer extends Character implements IHealer {
    public constructor(name: string, race: RaceType) {
        super(
            name,
            CharacterConstants.HEALER_BASE_HEALTH_POINTS,
            CharacterConstants.HEALER_BASE_ARMOR_POINTS,
            CharacterConstants.HEALER_ABILITY_POINTS,
            new Backpack(),
            race
        );
    }

    public heal(character: ICharacter): void {
        this.isAliveValidate();
        character.isAliveValidate();

        if (character.race !== this.race) {
            throw new Error(CharacterConstants.GET_CANNOT_HEAL_ENEMIES_ERROR_MESSAGE());
        }

        character.health = Math.min(character.baseHealth, character.health + this.abilityPoints);
    }
}
