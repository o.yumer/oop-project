import { inject, injectable } from 'inversify';
import { TYPES } from '../common/types';
import { IItemFactory } from '../core';
import { IRepository } from '../data';
import { IItem } from '../models';
import { CommandsConstants } from './../common/constants';
import { ICommand } from './contracts';

@injectable()
export class AddItemToPool implements ICommand {
    private readonly _itemFactory: IItemFactory;
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.itemFactory) itemFactory: IItemFactory,
                       @inject(TYPES.repository) repository: IRepository) {
        this._itemFactory = itemFactory;
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const [itemName] = parameters;

        const item: IItem = this._itemFactory.createItem(itemName);

        this._repository.itemPool.push(item);

        return CommandsConstants.GET_ADD_ITEM_TO_POOL_SUCCESS_MESSAGE(itemName);
    }
}
