import { inject, injectable } from 'inversify';
import { TYPES } from '../common/Types';
import { IRepository } from '../data';
import { CommandsConstants } from './../common/constants';
import { ICharacter } from './../models';
import { ICommand } from './contracts';

@injectable()
export class GetStats implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(): string {
        let msg: string = CommandsConstants.GET_FINAL_STATS_MESSAGE();
        msg += '\r\n';

        this._repository.party
            .sort((a: ICharacter, b: ICharacter) => b.health - a.health)
            .forEach((c: ICharacter) => {
                msg += CommandsConstants.GET_STATS_SUCCESS_MESSAGE(c);
                msg += '\r\n';
            });

        return msg.trim();
    }
}
