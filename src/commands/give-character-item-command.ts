import { inject, injectable } from 'inversify';
import { TYPES } from '../common/Types';
import { IRepository } from '../data';
import { ICharacter,
         IItem
} from '../models';
import { CommandsConstants } from './../common/constants';
import { ICommand } from './contracts';

@injectable()
export class GiveCharacterItem implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const[giverName, receiverName, itemName] = parameters;

        const giver: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === giverName);

        if (!giver) {
            throw new Error(CommandsConstants.GET_GIVER_NOT_FOUND_ERROR_MESSAGE(giverName));
        }

        const receiver: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === receiverName);

        if (!receiver) {
            throw new Error(CommandsConstants.GET_RECEIVER_NOT_FOUND_ERROR_MESSAGE(receiverName));
        }

        const item: IItem = giver.bag.getItem(itemName);

        giver.giveCharacterItem(item, receiver);

        return CommandsConstants.GET_GIVE_CHARACTER_ITEM_SUCCESS_MESSAGE(giverName, receiverName, itemName);
    }
}
