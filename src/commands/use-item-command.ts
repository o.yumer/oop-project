import { inject, injectable } from 'inversify';
import { TYPES } from '../common/Types';
import { IRepository } from '../data';
import { CommandsConstants } from './../common/constants';
import { ICharacter,
         IItem
} from './../models';
import { ICommand } from './contracts';

@injectable()
export class UseItem implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const [characterName, itemName] = parameters;

        const character: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === characterName);

        if (!character) {
            throw new Error(CommandsConstants.GET_CHARACTER_NOT_FOUND_ERROR_MESSAGE(characterName));
        }

        const item: IItem = character.bag.getItem(itemName);

        character.useItem(item);

        return CommandsConstants.GET_USE_ITEM_SUCCESS_MESSAGE(characterName, itemName);
    }
}
