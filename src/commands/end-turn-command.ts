import { inject, injectable } from 'inversify';
import { TYPES } from '../common/Types';
import { IRepository } from '../data';
import { CommandsConstants } from './../common/constants';
import { ICharacter } from './../models';
import { ICommand } from './contracts';

@injectable()
export class EndTurn implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(): string {
        const aliveCharacters: ICharacter[] = this._repository.party
            .filter((c: ICharacter) => c.isAlive);

        let msg: string = '';

        aliveCharacters.forEach((c: ICharacter) => {
            const healthBeforeRest: number = c.health;
            c.rest();
            const healthAfterRest: number = c.health;

            msg += CommandsConstants.GET_REST_SUCCESS_MESSAGE(c, healthBeforeRest, healthAfterRest);
        });

        return msg.trim();
    }
}
