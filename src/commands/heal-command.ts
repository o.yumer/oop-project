import { inject, injectable } from 'inversify';
import { TYPES } from '../common/Types';
import { IRepository } from '../data';
import { CommandsConstants } from './../common/constants';
import { Healer,
         ICharacter
} from './../models';
import { ICommand } from './contracts';

@injectable()
export class Heal implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const [healerName, receiverName] = parameters;

        const healer: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === healerName);

        if (!healer) {
            throw new Error(CommandsConstants.GET_HEALER_NOT_FOUND_ERROR_MESSAGE(healerName));
        }

        const receiver: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === receiverName);

        if (!receiver) {
            throw new Error(CommandsConstants.GET_RECEIVER_NOT_FOUND_ERROR_MESSAGE(receiverName));
        }

        if (!(healer instanceof Healer)) {
            throw new Error(CommandsConstants.GET_CANNOT_HEAL_ERROR_MESSAGE(healerName));
        }

        healer.heal(receiver);

        return CommandsConstants.GET_HEAL_SUCCESS_MESSAGE(healer, receiver);
    }
}
