import { inject, injectable } from 'inversify';
import { TYPES } from '../common/types';
import { IRepository } from '../data';
import { ICharacter,
         IItem
} from '../models';
import { CommandsConstants, ItemsConstants } from './../common/constants';
import { ICommand } from './contracts';

@injectable()
export class PickUpItem implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const [characterName] = parameters;

        const character: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === characterName);

        if (!character) {
            throw new Error(CommandsConstants.GET_CHARACTER_NOT_FOUND_ERROR_MESSAGE(characterName));
        }

        if (!this._repository.itemPool.length) {
            throw new Error(CommandsConstants.GET_NO_ITEMS_IN_POOL_ERROR_MESSAGE());
        }

        const item: IItem = <IItem>this._repository.itemPool.pop();

        character.receiveItem(item);

        return CommandsConstants.GET_PICK_UP_ITEM_SUCCESS_MESSAGE(characterName, item);
    }
}
