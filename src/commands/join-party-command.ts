import { inject, injectable } from 'inversify';
import { TYPES } from '../common/types';
import { ICharacterFactory } from '../core';
import { IRepository } from '../data';
import { CharacterType,
         ICharacter,
         RaceType
} from '../models';
import { CharacterConstants,
         CommandsConstants
} from './../common/constants';
import { ICommand } from './contracts';

@injectable()
export class JoinParty implements ICommand {
    private readonly _repository: IRepository;
    private readonly _characterFactory: ICharacterFactory;

    public constructor(@inject(TYPES.repository) repository: IRepository,
                       @inject(TYPES.characterFactory) characterFactory: ICharacterFactory) {
        this._repository = repository;
        this._characterFactory = characterFactory;
    }

    public execute(parameters: string[]): string {
        const [raceType, characterType, name] = parameters;

        if (!(raceType in RaceType)) {
            throw new Error(CharacterConstants.GET_INVALID_RACE_ERROR_MESSAGE(raceType));
        }

        if (!(characterType in CharacterType)) {
            throw new Error(CharacterConstants.GET_INVALID_CHARACTER_TYPE_ERROR_MESSAGE(characterType));
        }

        const character: ICharacter = this._characterFactory
            .createCharacter(characterType, name, RaceType[<keyof typeof RaceType>raceType]);

        this._repository.party.push(character);

        return CommandsConstants.GET_JOIN_PARTY_SUCCESS_MESSAGE(name);
    }
}
