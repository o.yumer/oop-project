import { inject, injectable } from 'inversify';
import { CommandsConstants } from '../common/constants/commands-constants';
import { TYPES } from '../common/Types';
import { IRepository } from '../data/contracts/repository';
import { ICharacter,
         Warrior
} from '../models';
import { ICommand } from './contracts/command';

@injectable()
export class Attack implements ICommand {
    private readonly _repository: IRepository;

    public constructor(@inject(TYPES.repository) repository: IRepository) {
        this._repository = repository;
    }

    public execute(parameters: string[]): string {
        const [attackerName, receiverName] = parameters;

        const attacker: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === attackerName);

        if (!attacker) {
            throw new Error(CommandsConstants.GET_ATTACKER_NOT_FOUND_ERROR_MESSAGE(attackerName));
        }

        const receiver: ICharacter | undefined = this._repository.party.find((c: ICharacter) => c.name === receiverName);

        if (!receiver) {
            throw new Error(CommandsConstants.GET_RECEIVER_NOT_FOUND_ERROR_MESSAGE(receiverName));
        }

        if (!(attacker instanceof Warrior)) {
            throw new Error(CommandsConstants.GET_CANNOT_ATTACK_ERROR_MESSAGE(attackerName));
        }

        attacker.attack(receiver);

        let message: string = CommandsConstants.GET_ATTACK_SUCCESS_MESSAGE(attacker, receiver);

        if (!receiver.isAlive) {
            message += '\r\n';
            message += CommandsConstants.GET_KILL_SUCCESS_MESSAGE(receiver);
        }

        return message;
    }
}
