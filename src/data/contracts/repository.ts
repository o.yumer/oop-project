import { ICharacter,
         IItem
} from '../../models';

export interface IRepository {
    party: ICharacter[];

    itemPool: IItem[];
}
