import { injectable } from 'inversify';
import { ICharacter,
         IItem
} from '../models';
import { IRepository } from './contracts';

@injectable()
export class Repository implements IRepository {
  private readonly _party: ICharacter[];
  private readonly _itemPool: IItem[];

  private constructor() {
    this._party = [];
    this._itemPool = [];
  }

  public get party(): ICharacter[] {
    return this._party;
  }

  public get itemPool(): IItem[] {
    return this._itemPool;
  }
}
