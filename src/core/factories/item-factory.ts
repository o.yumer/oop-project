import { injectable } from 'inversify';
import { ItemsConstants } from '../../common/constants';
import { ArmorRepairKit,
         HealthPotion,
         IItem,
         PoisonPotion
} from '../../models';
import { IItemFactory } from './contracts';

@injectable()
export class ItemFactory implements IItemFactory {
    public createItem(itemName: string): IItem {
        switch (itemName) {
            case 'PoisonPotion': {
                return new PoisonPotion();
            }
            case 'HealthPotion': {
                return new HealthPotion();
            }
            case 'ArmorRepairKit': {
                return new ArmorRepairKit();
            }
            default: {
                throw new Error(ItemsConstants.GET_NO_SUCH_ITEM_ERROR_MESSAGE(itemName));
            }
        }
    }
}
