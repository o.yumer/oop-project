import { injectable } from 'inversify';
import { CharacterConstants } from '../../common/constants';
import { Healer,
         ICharacter,
         RaceType,
         Warrior
} from '../../models';
import { ICharacterFactory } from './contracts';

@injectable()
export class CharacterFactory implements ICharacterFactory {
    public createCharacter(characterType: string, name: string, race: RaceType): ICharacter {
        switch (characterType) {
            case 'Warrior': {
                return new Warrior(name, race);
            }
            case 'Healer': {
                return new Healer(name, race);
            }
            default: {
                throw new Error(CharacterConstants.GET_INVALID_CHARACTER_TYPE_ERROR_MESSAGE(characterType));
            }
        }
    }
}
