import { ICharacter,
         RaceType
} from '../../../models';

export interface ICharacterFactory {
    createCharacter(characterType: string, name: string, race: RaceType): ICharacter;
}
