import { IItem } from '../../../models';

export interface IItemFactory {
    createItem(itemName: string): IItem;
}
