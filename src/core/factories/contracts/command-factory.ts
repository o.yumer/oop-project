import { ICommand } from '../../../commands';

export interface ICommandFactory {
    getCommand(commandName: string): ICommand;
}
