# Aliens vs Terrestrials Console Game by ROM
## Team details
### Team name: 
- ROM
### Team members: 
- Maria Dineva (Username in telerikacademy.com - maria.dineva)
- Osman Yumer (Username in telerikacademy.com - oyumer)
- Rosen Ivanov (Username in telerikacademy.com - rosen.ivanov)
## Gitlab repository 
- Link: https://gitlab.com/o.yumer/oop-project
## Used assets in the project
- TypeScript
- TSLint
- InversifyJS
- Jest
- GitLab
## Used design patterns in the project
- Factory
- Command
- Singleton
### Project Information
1. Run npm install
2. With Ctr+F5 you can get the output in your debug console.
3. All the input is stored in the commands.txt file.
4. With npm test you can run the tests.
5. The project runs only in local environment.
### Project Description:
We have tried to follow the OOP principles and to apply the best practices we have learnt over the last few weeks in order to create the basic structure of a console game with the following rules:
- There are two races who fight against each other - Aliens and Terrestrials.
- Each race has two types of characters - Warriors and Healers.
- Each character has a name, Health Points (Warrior has 100, Healer has 50 upon initialization), Armor Points (Warrior has 50, Healer has 25 upon initialization, Ability Points (40 points for both characters),a bag ( Warrior can carry a small bag of type Satchel and each Healer can carry a bigger bag of type Backpack) and a race (Alien or Terrestrial, as mentioned above).
- A Warrior can attack only a character from the other race.
- A Healer can heal only a character from his own race.
- In their bags the characters can carry three sorts of items: an Armor Repair Kit, which fully restores the character's armor points when used; a Health Potion, which adds 20 points to the character's health when used; and a Poison Potion which takes 20 points damage when used on the opponent.

Our console reader reads some commands from a .txt file and outputs them in the console. These commands make up the main logic of the game. The commands are as follow:
- JoinParty - it creates a new character with a certain name, type and race and adds him to the database.
- Attack - a character can attack an opponent with force which equals his ability points. The attacked takes damage by getting his armor points reduced and if there are none - by getting his health points reduced. If his health points reach 0, the character dies.
- AddItemToPool - creates one of the three items and adds it to the pool so that the characters can pick it and use it afterwards.
- PickUpItem - a character can pick up the last item that was added to the pool.
- UseItem - a character can use the item he has picked to cure himself, i.e. the Armor Repair Kit to restore his armor points, and the Health Potion to add up to his health points.
- UseItemOn = a character can use the Poison Potion on his opponent.
- GiveCharacterItem - a character can give an item to one of his teammates.
- Heal - a Healer heals his teammates by increasing their health points with the amount of his ability points.
- EndTurn - when the turn end, all characters rest, i.e. they recharge. Their health increases with 20 percent of their base health points(the health points they have upon initialization).
- GetStats - this command lists all the characters in the game and displays their health points, armor points and status(alive/dead).
## Problems we had before we started refactoring and testing
Some of the problems we met when we started refactoring concern minor tslint mistakes and logic in the input .txt file. Also, we tried to make the code more readable by introducing blank lines between some blocks of code which did not follow the same idea. We added some more validations where we found necessary. When we started testing, we also found out that the logic we had in some of the classes was not quite right so we had to slightly change the code at some places.